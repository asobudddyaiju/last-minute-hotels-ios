
import 'package:hive/hive.dart';
part 'age_model.g.dart';
@HiveType(typeId: 0)
class Age extends HiveObject {
  @HiveField(0)
  List<List<String>> age;
  Age({this.age});
}